import { Plugin } from 'ckeditor5/src/core';
import SmallCommand from './smallcommand';

// For code lisibility and easy name changes.
export const COMMAND_NAME = 'small';
export const MODEL_ATTR_NAME = 'small';

export default class Smallediting extends Plugin {
  /**
   * @inheritDoc
   */
  static get pluginName() {
    return 'SmallEditing';
  }

  /**
   * @inheritDoc
   */
  init() {
    const editor = this.editor;
    editor.model.schema.extend('$text', { allowAttributes: MODEL_ATTR_NAME });
    editor.model.schema.setAttributeProperties(MODEL_ATTR_NAME, {
      isFormatting: false,
      copyOnEnter: true,
    });

    editor.conversion.attributeToElement({
      model: MODEL_ATTR_NAME,
      view: 'small',
    });

    editor.commands.add(COMMAND_NAME, new SmallCommand(editor));
  }
}
