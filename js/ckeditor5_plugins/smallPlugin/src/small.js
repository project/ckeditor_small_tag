/**
 * Inspired from ckeditor5-basic-styles/subscript.
 */

import SmallEditing from './smallediting';
import SmallUI from './smallui';
import { Plugin } from 'ckeditor5/src/core';

export default class Small extends Plugin {
  static get requires() {
    return [SmallEditing, SmallUI];
  }
}
