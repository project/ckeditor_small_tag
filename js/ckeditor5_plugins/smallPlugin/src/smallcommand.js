import { Command } from 'ckeditor5/src/core';
import { MODEL_ATTR_NAME } from './smallediting';

// @See ckeditor-basic-styles/src/attributecommand.js
// I guess there's a way to build a plugin which directly uses AttributeCommand
// but I could not find how
// (maybe
// https://www.drupal.org/docs/core-modules-and-themes/core-modules/ckeditor-5-module/ckeditor-5-development?).
// So I make a specific command here for small.
export default class SmallCommand extends Command {

  refresh() {
    const model = this.editor.model;
    const doc = model.document;

    this.value = this._getValueFromFirstAllowedNode();
    this.isEnabled = model.schema.checkAttributeInSelection(doc.selection, MODEL_ATTR_NAME);
  }

  execute(options = {}) {
    const model = this.editor.model;
    const doc = model.document;
    const selection = doc.selection;
    const value = !this.value;

    model.change(writer => {
      if (selection.isCollapsed) {
        if (value) {
          writer.setSelectionAttribute(MODEL_ATTR_NAME, true);
        }
        else {
          writer.removeSelectionAttribute(MODEL_ATTR_NAME);
        }
      }
      else {
        const ranges = model.schema.getValidRanges(selection.getRanges(), MODEL_ATTR_NAME);

        for (const range of ranges) {
          if (value) {
            writer.setAttribute(MODEL_ATTR_NAME, value, range);
          }
          else {
            writer.removeAttribute(MODEL_ATTR_NAME, range);
          }
        }
      }
    });
  }

  _getValueFromFirstAllowedNode() {
    const model = this.editor.model;
    const schema = model.schema;
    const selection = model.document.selection;

    if (selection.isCollapsed) {
      return selection.hasAttribute(MODEL_ATTR_NAME);
    }

    for (const range of selection.getRanges()) {
      for (const item of range.getItems()) {
        if (schema.checkAttribute(item, MODEL_ATTR_NAME)) {
          return item.hasAttribute(MODEL_ATTR_NAME);
        }
      }
    }

    return false;
  }
}
