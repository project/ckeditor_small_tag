# CKeditor Small Tag

CKeditor Small Tag module adds a CKEditor5 button to put the selected text into
a small HTML element. It works in the same way as the bold, italic or strike-through
CKE buttons.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/ckeditor_small_tag).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/ckeditor_small_tag).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-modules).


## Configuration

- Go to Administration > Extend and enable the module.
- Go to _admin/config/content/formats_.
- Click on the configure button of the format in which you want to add the small tag button.
- Then Drag and drop the small tag button in Active toolbar.


## Maintainers

Gaël Gosset - [GaëlG](https://www.drupal.org/u/ga%C3%ABlg)
